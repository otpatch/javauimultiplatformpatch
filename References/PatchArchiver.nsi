; Java Launcher
;--------------

;You want to change the next four lines
Name "Multi Platform Patch Archive Generator"
Caption "Patch Archiver"
Icon "PatchArchiver.ico"
OutFile "xECMPatchArchiver.exe"

SilentInstall silent
AutoCloseWindow false
ShowInstDetails show

;You want to change the next two lines too
!define CLASSPATH ".;dist/lib/;dist/lib/dos2unix/"
!define CLASS "org.ot.multiplatformpatch.MultiPlatformPatch"

Section
  Call GetJRE
  Pop $R0
  ; MessageBox MB_OK "JRE Version is $R0"
  ; change for your purpose (-jar etc.)
  StrCpy $0 '"$R0" -classpath "${CLASSPATH}" ${CLASS}'
  MessageBox MB_OK "classpath is ${CLASSPATH} ${CLASS}"
  SetOutPath $EXEDIR
  ; MessageBox MB_OK "Executable directory is $EXEDIR"
  ExecWait $0
SectionEnd

Function GetJRE
;
;  Find JRE (javaw.exe)
;  1 - in .\jre directory (JRE Installed with application)
;  2 - in JAVA_HOME environment variable
;  3 - in the registry
;  4 - assume javaw.exe in current dir or PATH

  Push $R0
  Push $R1

  ClearErrors
  StrCpy $R0 "$EXEDIR\jre\bin\java.exe"
  IfFileExists $R0 JreFound
  StrCpy $R0 ""

  ClearErrors
  ReadEnvStr $R0 "JAVA_HOME"
  StrCpy $R0 "$R0\bin\java.exe"
  IfErrors 0 JreFound

  ClearErrors
  ReadRegStr $R1 HKLM "SOFTWARE\JavaSoft\Java Runtime Environment" "CurrentVersion"
  ReadRegStr $R0 HKLM "SOFTWARE\JavaSoft\Java Runtime Environment\$R1" "JavaHome"
  StrCpy $R0 "$R0\bin\java.exe"

  IfErrors 0 JreFound
  StrCpy $R0 "java.exe"

 JreFound:
  Pop $R1
  Exch $R0
FunctionEnd