/* 
 * Copyright (C) 2017 Suresh Raju Pilli.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package org.ot.multiplatformpatch.utils;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.ot.multiplatformpatch.reader.ExcludeFileFilter;
import static org.ot.multiplatformpatch.utils.Constants.ARCHIVE_CREATION_FAILURE;
import org.rauschig.jarchivelib.ArchiveFormat;
import org.rauschig.jarchivelib.Archiver;
import org.rauschig.jarchivelib.ArchiverFactory;
import org.rauschig.jarchivelib.CompressionType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author khasim, psrdotcom
 */
public class FileUtils {

    // Local variables to use
    public static final String CS64 = "_CS64_";
    public static final String FOR = "for";

    /**
     * Copies all the folder and files from a given folder
     *
     * @param src source folder
     * @param dest destination/target folder
     *
     */
    public static void copyFolder(File src, File dest) {
        try {
            if (Files.exists(src.toPath())) {
                if (src.isDirectory()) {
                    //if directory not exists, create it
                    if (!dest.exists()) {
                        dest.mkdir();
                    } else {
                        // if directory already exists, delete and recreate it 
                        deleteDirectory(dest.getAbsolutePath());
                        dest.mkdir();
                    }

                    //list all the directory contents
                    String files[] = src.list();

                    for (String file : files) {
                        //construct the src and dest file structure
                        File srcFile = new File(src, file);
                        File destFile = new File(dest, file);
                        //recursive copy
                        copyFolder(srcFile, destFile);
                    }
                } else {
                    //if file, then copy it
                    Files.copy(src.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
                }
            } else {
                UiUtils.showErrorMessage(null, Constants.SOURCE_FOLDER_DOESNT_EXISTS, Constants.SOURCE_NOT_FOUND);
            }
        } catch (IOException e) {
            UiUtils.showErrorMessage(null, "Failed to create Folder Structure", "Copy Failure");
        }
    }

    /**
     * Copy source directory contents and create OS directories
     *
     * @param src the source directory path
     * @param OsArray the OSes Array
     * @return false - if copying or directory creation fails, true - otherwise
     */
    public static boolean copyAndCreateDirs(String src, String[] OsArray) {
        for (String os : OsArray) {
            copyFolder(new File(src), new File(src + Constants.PARENT_DIR_SEPARATOR + os));
        }
        return true;
    }

    /**
     * Zip compress of folder and its contents
     *
     * @param archiveName the name of the archive
     * @param source the source folder path
     * @return false - if fails to compress and create zip file, true -
     * otherwise
     */
    public static boolean compressZipFile(String archiveName, String source) {
        return compressZipFile(archiveName, source, source + Constants.PARENT_DIR_SEPARATOR);
    }

    /**
     * Zip compress of folder and its contents
     *
     * @param archiveName the name of the archive
     * @param source the source folder path
     * @param destination the destination folder path
     * @return false - if fails to compress and create zip file, true -
     * otherwise
     */
    public static boolean compressZipFile(String archiveName, String source, String destination) {
        try {
            return zipDir(archiveName, source, destination);
        } catch (Exception ex) {
            Logger.getLogger(FileUtils.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    /**
     * Tar GunZip compress of folder and its contents
     *
     * @param archiveName the name of the archive
     * @param source the source folder path
     * @return false - if fails to compress and create zip file, true -
     * otherwise
     */
    public static boolean compressTarGunZipFile(String archiveName, String source) {
        return compressTarGunZipFile(archiveName, source, source + Constants.PARENT_DIR_SEPARATOR);
    }

    /**
     * Tar GunZip compress of folder and its contents
     *
     * @param archiveName the name of the archive
     * @param source the source folder path
     * @param destination the destination folder path
     * @return false - if fails to compress and create zip file, true -
     * otherwise
     */
    public static boolean compressTarGunZipFile(String archiveName, String source, String destination) {
        Archiver archiver = ArchiverFactory.createArchiver(ArchiveFormat.TAR, CompressionType.GZIP);
        return compressFiles(archiveName, source, destination, archiver);
    }

    /**
     * Archive files
     *
     * @param archiveName the archive file name
     * @param sourcePath the source folder or file path
     * @param destPath the destination file or folder path
     * @param archiver the compress type
     * @return true - success, false - failure
     */
    public static boolean compressFiles(String archiveName, String sourcePath, String destPath, Archiver archiver) {
        return compressFiles(archiveName, new File(sourcePath), new File(destPath), archiver);
    }

    /**
     * Archive files
     *
     * @param archiveName the archive file name
     * @param source the source folder or file
     * @param destination the destination file or folder
     * @param archiver the compress type
     * @return true - success, false - failure
     */
    public static boolean compressFiles(String archiveName, File source, File destination, Archiver archiver) {
        try {
            File destFolder = new File(destination.getParent()).getParentFile().getAbsoluteFile().getParentFile();

            // If archive already exists, delete it
            if (deleteFile(destFolder, archiveName, archiver.getFilenameExtension())) {
                File archive = archiver.create(archiveName, destFolder, source);
                if (archive != null && archive.isFile()) {
                    //System.out.println(archive.getName() + " file created");
                } else {
                    UiUtils.showErrorMessage(null, archiveName + "." + archiver.getFilenameExtension() + " creation failed", ARCHIVE_CREATION_FAILURE);
                    return false;
                }
            } else {
                UiUtils.showErrorMessage(null, Constants.DEST_FOLDER_DOESNT_EXISTS, Constants.FOLDER_MISSING);
                return false;
            }
        } catch (IOException ex) {
            UiUtils.showErrorMessage(null, archiveName + "." + archiver.getFilenameExtension() + " creation failed", ARCHIVE_CREATION_FAILURE);
            return false;
        }
        return true;
    }

    /**
     * Get Parsed XML DOM
     *
     * @param xmlString the XML String
     * @return Document the parsed DOM
     * @throws SAXException
     * @throws IOException
     * @throws ParserConfigurationException
     */
    public static Document getParsedDOM(String xmlString) throws SAXException, IOException, ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(xmlString));
        Document doc = docBuilder.parse(is);
        doc.getDocumentElement().normalize();
        return doc;
    }

    /**
     * Get Parsed XML DOM
     *
     * @param xmlFile the XML file with patch headers
     * @return Document the parsed DOM
     * @throws SAXException
     * @throws IOException
     * @throws ParserConfigurationException
     */
    public static Document getParsedDOM(File xmlFile) throws SAXException, IOException, ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = dbf.newDocumentBuilder();
        Document doc = docBuilder.parse(xmlFile);
        doc.getDocumentElement().normalize();
        return doc;
    }

    /**
     * Get Patch Names from Patch headers
     *
     * @param dom the document parsed object
     * @param patchHdr the patch header XML response
     * @param fileName the XML tag to search
     * @return String[] the patch name string array
     */
    public static String[] getPatchNames(Document dom, String patchHdr, String fileName) {
        NodeList nodeList = dom.getElementsByTagName(patchHdr);
        String patch[] = new String[nodeList.getLength()];
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                patch[i] = getValue(fileName, element);
            }
        }
        return patch;
    }

    /**
     * Get patch header content
     *
     * @param dom the DOM
     * @param xmlElement the xml element to search for
     * @return HashMap<String, String> the patch file name and its respective
     * patch header, and the key, value pair of patch number
     */
    public static HashMap<String, String> getPatchNameHeaderContent(Document dom, String xmlElement) {
        NodeList nodeList = dom.getElementsByTagName(xmlElement);
        String patchHdrs = null;

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                patchHdrs = (String) element.getTextContent();
            }
        }

        // Parse the string here
        return parsePatchHeaders(patchHdrs);
    }

    /**
     * Get patch header content
     *
     * @param dom the DOM
     * @param patchHdr the Patch Header String
     * @param fileName the File name
     * @param hdrContent the Header content
     * @return HashMap of patch file name and its respective patch header, the
     * last key, value pair is patch number
     */
    public static HashMap<String, String> getPatchNameHeaderContent(Document dom, String patchHdr, String fileName, String hdrContent) {
        NodeList nodeList = dom.getElementsByTagName(patchHdr);
        HashMap<String, String> nameHdrMap = new HashMap<>();
        String key, val = null;
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                key = getValue(fileName, element);
                val = getValue(hdrContent, element);
                nameHdrMap.putIfAbsent(key, val);
            }
        }
        if (val != null) {
            nameHdrMap.put(Constants.PATCH_NO, getPatchNumber(val));
        }
        return nameHdrMap;
    }

    /**
     * Get the value of the element
     *
     * @param tag the tag to search
     * @param element the element
     * @return String the element tag value
     */
    private static String getValue(String tag, Element element) {
        NodeList nodes = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = (Node) nodes.item(0);
        return node.getNodeValue();
    }

    /**
     * Get patch number from the patch header
     *
     * @param patchHdr the patch header
     * @return String the patch number
     */
    private static String getPatchNumber(String patchHdr) {
        String firstLine;
        try (Scanner scanner = new Scanner(patchHdr)) {
            firstLine = scanner.nextLine().split(" ")[2];
        }
        return firstLine.trim();
    }

    /**
     * Get all the files/folder names from folder and sub-folders excluding the
     * file extensions specified
     *
     * @param dirName the Directory name
     * @param excludeFiles the exclude file extension list
     * @return List the list of files from folder and its sub-folders
     */
    public static List<File> listAllFiles(String dirName, File excludeFiles) {
        File directory = new File(dirName);

        List<File> resultList = new ArrayList<>();

        // Get all the files from folder
        File[] fList = directory.listFiles(new ExcludeFileFilter(excludeFiles));
        resultList.addAll(Arrays.asList(fList));
        for (File file : fList) {
            if (file.isDirectory()) {
                // If any sub-folder, then call recursive function
                resultList.addAll(listAllFiles(file.getAbsolutePath(), excludeFiles));
            }
        }
        return resultList;
    }

    /**
     * Get Patch Folder path
     *
     * @param dir the parent directory
     * @param os the OS name
     * @return File the folder of patch
     */
    public static File getPatchFolderPath(String dir, String os) {
        return new File(dir + Constants.PARENT_DIR_SEPARATOR + os + "\\patch");
    }

    /**
     * Get Text File Name Filter
     *
     * @return FilenameFilter the text file name filter
     */
    public static FilenameFilter getTextFileNameFilter() {
        FilenameFilter textFileFilter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return (name.toLowerCase().endsWith(".txt") && name.toLowerCase().startsWith("pat"));
            }
        };
        return textFileFilter;
    }

    /**
     * Get the list of patch text files from patch folder
     *
     * @param patchFolder the patch folder
     * @param textFileFilter the text file filter
     * @return File[] the list of text files
     */
    public static File[] getPatchFileNameList(File patchFolder, FilenameFilter textFileFilter) {
        return patchFolder.listFiles(textFileFilter);
    }

    /**
     * Delete directory files and sub folders
     *
     * @param osDir the directory path to delete
     * @return deletions status
     */
    public static boolean deleteDirectory(String osDir) {
        try {
            Path dirPath = Paths.get(osDir);
            Files.walkFileTree(dirPath, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attr) throws IOException {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException ioe) throws IOException {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException ex) {
            Logger.getLogger(FileUtils.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    /**
     * Delete file
     *
     * @param destFolder the destination folder
     * @param archiveName the file name
     * @param filenameExtension the file extension
     * @return file deletion status
     */
    public static boolean deleteFile(File destFolder, String archiveName, String filenameExtension) {
        return deleteFile(destFolder, archiveName + filenameExtension);
    }

    /**
     * Delete file
     *
     * @param destFolder the destination folder
     * @param filename the filename
     * @return file deletion status
     */
    public static boolean deleteFile(File destFolder, String filename) {
        // Check dest folder
        if (Files.exists(destFolder.toPath())) {
            // Check archive file exists
            Path filePath = Paths.get(destFolder.getAbsolutePath() + "\\" + filename);
            if (Files.exists(filePath)) {
                try {
                    // delete the file
                    Files.delete(filePath);
                } catch (IOException ex) {
                    // Do nothing, as we are just verifying the file existance
                }
            } else {
                //System.out.println("multiplatformpatch.utils.FileUtils.deleteFile()" + filePath);
                // Do nothing, as we are just verifying the file existance
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * Get archive file names
     *
     * @param dirPath the directory to search for
     * @return String[] the archive file name array
     */
    public static String[] getArchiveFiles(String dirPath) {
        return new File(dirPath).list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return (name.toLowerCase().endsWith(".gz") || name.toLowerCase().endsWith(".zip"));
            }
        });
    }

    private static HashMap<String, String> parsePatchHeaders(String patchHdrs) {
        HashMap<String, String> nameHdrMap = new HashMap<>();
        String key = null;

        // Parse by OS
        String[] osHeaders = patchHdrs.split("\n\n\n");

        // Extract patch number
        String patchNo = getPatchNumber(osHeaders[0]).toLowerCase();
        nameHdrMap.put(Constants.PATCH_NO, patchNo);

        // Iterate and make OS and 
        for (String hdr : osHeaders) {
            key = frameFileName(getHdrOS(hdr), patchNo);
            nameHdrMap.put(key, hdr);
        }
        return nameHdrMap;
    }

    /**
     * Get the OS from header
     *
     * @param hdr the header content
     * @return String the OS name
     */
    static String getHdrOS(String hdr) {
        String line = hdr.split(Constants.UNIX_NEW_LINE)[4];
        return line.substring(line.indexOf(FOR) + 3).trim();

    }

    /**
     * Frame the file name with
     *
     * @param os the OS name
     * @param patchNo the patch number to prepend
     * @return String the OS specific filename
     */
    private static String frameFileName(String os, String patchNo) {
        String fileName = patchNo.concat(CS64);

        if (os.equals(Constants.WINDOWS)) {
            return fileName.concat(Constants.WINDOWS_FILENAME);
        } else if (os.startsWith(Constants.LINUX)) {
            if (patchNo.startsWith(Constants.CS16, 3)) {
                return fileName.concat(Constants.LINUX_16_FILENAME);
            } else {
                return fileName.concat(Constants.LINUX_1052_FILENAME);
            }
        } else if (os.startsWith(Constants.SOLARIS)) {
            return fileName.concat(Constants.SOLARIS_FILENAME);
        } else {
            return null;
        }
    }

    /**
     * Archive the folder contents in zip format
     *
     * @param zipFileName the destination zip file name
     * @param srcDir the source directory path
     * @param destDir the destination directory path to save the zip file
     * @return boolean the archive file creation result
     * @throws Exception
     */
    public static boolean zipDir(String zipFileName, String srcDir, String destDir) throws Exception {
        // Run 7ZIP command
        String destPath = new File(new File(destDir).getParent()).getParentFile().getAbsoluteFile().getParentFile().getCanonicalPath();
        String srcPath = new File(srcDir).getCanonicalPath();
        String command = Constants.SEVEN_ZIP_COMMAND + destPath + "\\" + zipFileName + ".zip " + srcPath + "\\*";
        return runCommand(command);
    }

    /**
     * Run the command
     *
     * @param command the command string
     * @return boolean the command execution result
     */
    public static boolean runCommand(String command) {
        try {
            Process proc = Runtime.getRuntime().exec(command);

            // Waiting process to complete
            boolean exitCode = proc.waitFor(100, TimeUnit.MILLISECONDS);
            if (!exitCode) {
                // Waiting for more time to complete
                proc.waitFor(500, TimeUnit.MILLISECONDS);
            }
        } catch (InterruptedException | IOException ex) {
            Logger.getLogger(FileUtils.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
}
