/* 
 * Copyright (C) 2017 Suresh Raju Pilli.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package org.ot.multiplatformpatch.utils;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author psrdotcom
 */
public class StringUtils {

    /**
     * Comma separated String to String Set
     *
     * @param csvString the comma separated string
     * @return String Set
     */
    public static Set<String> stringToSet(String csvString) {
        Set<String> set = new HashSet<>(Arrays.asList(csvString.replace(" ", "").split(",")));
        return set;
    }

    /**
     * Get File Extension
     *
     * @param file the file name
     * @return String the file extension
     */
    public static String getFileExtension(File file) {
        if (file != null) {
            String fileName = file.getName();
            if (fileName.length() > 0 && fileName.contains(".")) {
                return fileName.substring(fileName.lastIndexOf(".") + 1);
            }
        }
        return null;
    }
}
