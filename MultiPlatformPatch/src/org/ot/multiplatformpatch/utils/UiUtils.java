/* 
 * Copyright (C) 2017 Suresh Raju Pilli.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package org.ot.multiplatformpatch.utils;

import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.ot.multiplatformpatch.reader.ExcludeFileFilter;
import org.ini4j.Ini;

/**
 *
 * @author psrdotcom
 */
public class UiUtils {

    /**
     * Show error message
     *
     * @param parent the parent component
     * @param msg the message to display
     * @param title the title of the message box
     */
    public static void showErrorMessage(Component parent, String msg, String title) {
        JOptionPane.showMessageDialog(parent, msg, title, JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Show warning message
     *
     * @param parent the parent component
     * @param msg the message to display
     * @param title the title of the message box
     */
    public static void showWarningMessage(Component parent, String msg, String title) {
        JOptionPane.showMessageDialog(parent, msg, title, JOptionPane.WARNING_MESSAGE);
    }

    /**
     * Show information message
     *
     * @param parent the parent component
     * @param msg the message to display
     * @param title the title of the message box
     */
    public static void showMessage(Component parent, String msg, String title) {
        JOptionPane.showMessageDialog(parent, msg, title, JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Get the file extensions which shall be excluded from file filter
     *
     * @param fileName the file to read
     * @return file extensions String array
     */
    public static Set<String> filesToExclude(File fileName) {
        Set<String> extns = new HashSet<>();
        try {
            if (fileName != null) {
                Ini file = new Ini(fileName);
                String extensions = file.get(Constants.EXCLUDE_FILE_EXTENSIONS, Constants.EXTENSIONS);
                if (extensions != null && extensions.length() > 0) {
                    extns = StringUtils.stringToSet(extensions);
                }
            } else {
                showErrorMessage(null, Constants.FILE_DOESNT_EXISTS, Constants.INVALID_INI_FILE);
            }
        } catch (IOException ex) {
            Logger.getLogger(ExcludeFileFilter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return extns;
    }
}
