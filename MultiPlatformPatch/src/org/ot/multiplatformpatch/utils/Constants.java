/* 
 * Copyright (C) 2017 Suresh Raju Pilli.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package org.ot.multiplatformpatch.utils;

import java.util.Map;

/**
 * This class has all the strings used in entire project
 *
 * @author psrdotcom
 */
public class Constants {

    public static final String Version = "Ver 1.0.1";
    public static final String INVALID_TASK_ID_MSG = "Please enter a valid CS DB Task Id";
    public static final String INVALID_TASK_ID_MSG_HDR = "Invalid Task Id";
    public static final String INVALID_PATCH_DIR_MSG = "Please select a valid directory with at least one file or folder";
    public static final String INVALID_PATCH_DIR_MSG_HDR = "Invalid Patch Directory";
    public static final String INVALID_PATCH_HDR_MSG = "Please enter a valid patch headers";
    public static final String INVALID_PATCH_HDR_MSG_HDR = "Invalid Patch Headers";
    public static final String EXCLUDE_FILE_EXTENSIONS_FILE_PATH = "src/org/ot/multiplatformpatch/data/ExcludeFileExtns.ini";
    public static final String DOS2UNIX_COMMAND = "cmd.exe /c .\\lib\\exes\\dos2unix.exe ";
    public static final String INVALID_INI_FILE = "Invalid .ini file";
    public static final String FILE_DOESNT_EXISTS = "File doesn't exists";
    public static final String EXTENSIONS = "Extensions";
    public static final String EXCLUDE_FILE_EXTENSIONS = "ExcludeFileExtensions";
    public static final String SOURCE_NOT_FOUND = "Source not found";
    public static final String SOURCE_FOLDER_DOESNT_EXISTS = "Source folder doesn't exists or folder has no contents";
    public static final String WINDOWS = "Windows";
    public static final String WINDOWS_FILENAME = "WIN";
    public static final String LINUX = "Linux";
    public static final String LINUX_1052_FILENAME = "LNXRH6";
    public static final String LINUX_16_FILENAME = "LNX";
    public static final String SOLARIS = "Solaris";
    public static final String SOLARIS_FILENAME = "SOL10";
    public static final String TMP = "_TMP";
    public static final String[] CS1052_OS_LIST = new String[]{Constants.WINDOWS + TMP, Constants.LINUX + TMP, Constants.SOLARIS + TMP};
    public static final String[] CS16_OS_LIST = new String[]{Constants.WINDOWS + TMP, Constants.LINUX + TMP};
    public static final String ARCHIVE_CREATION_FAILURE = "Archive creation failure";
    public static Map<String, String> ARCHIVE_LIST = null;
    public static final String ARRAY_OF_PATCH_HEADER = "ArrayOfPatchHeader";
    public static final String PATCH_HEADER = "PatchHeader";
    public static final String FILE_NAME = "fileName";
    public static final String HEADER_CONTENT = "headerContent";
    public static final String USERNAME = "dummy";
    public static final String BASE_URL = "http://10.5.18.191/PatchAdmin/WebServices/Task_Service.asmx";
    public static final String PATCH_HEADER_XML_FILENAME = "PatchHeader.xml";
    public static final String UTF8_CHARSET = "UTF-8";
    public static final String PATCH_NO = "PatchNumber";
    public static final String PARENT_DIR_SEPARATOR = "\\..\\";
    public static final String MULTIPLE_PATCH_FILE_ERROR_TITLE = "Multiple patch file";
    public static final String MULTIPLE_PATCH_FILE_ERROR_MSG = "Make sure you have only one patch text file in patch folder";
    public static final String PATCH_HEADER_READING_PROBLEM_TITLE = "Patch header reading problem";
    public static final String UNABLE_TO_READ_PATCH_HEADERS_MSG = "Unable to read patch headers";
    public static final String PATCH_HEADER_MISSING_TITLE = "Patch header missing";
    public static final String NO_PATCH_HEADERS_FOUND_MSG = "No Patch headers found";
    public static final String PATCH_HEADER__FAILURE_TITLE = "Patch Header Failure";
    public static final String CREATE_OR_UPDATE_PATCH_HEADERS_FAILED_MSG = "Create or Update patch headers failed";
    public static final String TXT_EXTN = ".txt";
    public static final String WINDOWS_NEW_LINE = "\r\n";
    public static final String UNIX_NEW_LINE = "\n";
    public static final String PATCH_GEN_SUCCESS = "Patch archives generated successfully.\n Do you want to generate another patch?";
    public static final String PATCH_GEN_FAILURE = "Patch archives generation failed.\n Do you want to try again?";
    public static final String PATCH_GEN_CONFIRM_TITLE = "Patch Generation Message";
    public static final String FOLDER_MISSING = "Folder missing";
    public static final String DEST_FOLDER_DOESNT_EXISTS = "Destination folder doesn't exists";
    public static final String CREATE_OS_FOLDERS = " OS Folders creation";
    public static final String DELETE_OF_OS_FOLDERS = "Deletion of OS folders";
    public static final String CREATE_WIN_ARCHIVE = "Windows archive creation";
    public static final String CREATE_LNX_ARCHIVE = "Linux archive creation";
    public static final String CREATE_SOL_ARCHIVE = "Solaris archive creation";
    public static final String ARCHIVE_CREATION_ERROR_TITLE = "Archive creation error";
    public static final String UNABLE_TO_CREATE_ARCHIVE_FILE = "Unable to create archive file";
    public static final String PARSE_PATCH_HEADER = " Parsing patch header";
    public static final String GET_PATCH_HEADERS_FROM_CS_DB = " Getting patch headers from CS DB";
    public static final String UPDATE_WIN_PATCH = "Update Windows patch";
    public static final String UPDATE_LNX_PATCH = "Update Linux patch";
    public static final String UPDATE_SOL_PATCH = "Update Solaris patch";
    public static final String ARCHIVE_ALREADY_EXISTS = "Archive already exists";
    public static final String SOURCE_ALREADY_HAS_ARCHIVES = "Archives already exists in source directory.\nDo you want to delete the archives?";
    public static final String GENERATING_PATCH_ARCHIVES = "Generating patch archives";
    public static final String PATCHHEADER_URL = "//ConstructPatchHeader?taskid=";
    public static final String SEVEN_ZIP_COMMAND = "cmd.exe /c .\\lib\\exes\\7z.exe a ";
    public static final String CS16 = "16";
}
