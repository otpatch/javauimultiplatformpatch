/* 
 * Copyright (C) 2017 Suresh Raju Pilli.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package org.ot.multiplatformpatch.utils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ot.multiplatformpatch.ui.HomeScreen;

/**
 *
 * @author psrdotcom
 */
public class PatchUtils {

    /**
     * Validate patch number
     *
     * @param taskId the CS DB Task Id
     * @return true - if patch number is valid, false - otherwise
     */
    public static boolean validateTaskId(String taskId) {
        if (taskId != null && taskId.length() == 4) {
            try {
                if (Integer.parseInt(taskId) > 0) {
                    return true;
                }
            } catch (NumberFormatException nfe) {
                return false;
            }
        }
        return false;
    }

    /**
     * Validate patch directory by checking for sub-folders and files
     *
     * @param dirPath the directory absolute path
     * @return false - if no files or folders in the path true - otherwise
     */
    public static boolean validatePatchDir(String dirPath) {
        boolean result = false;
        if (dirPath != null) {
            File file = new File(dirPath);
            if (file.isDirectory()) {
                if (file.list().length > 0) {
                    return true;
                }
            }
        }
        return result;
    }

    /**
     * Convert DOS files to Unix file by changing EoL
     *
     * @param dirPath the directory path
     * @param excludeFiles the list of exclude files
     * @return status of conversion
     */
    public static boolean eoLchanges(String dirPath, File excludeFiles) throws InterruptedException {
        try {
            List<File> fileList = FileUtils.listAllFiles(dirPath, excludeFiles);
            // Perform EoL changes
            for (File f : fileList) {
                String command = Constants.DOS2UNIX_COMMAND + "\"" + f.getAbsolutePath() + "\"";
                Process proc = Runtime.getRuntime().exec(command);

                // Waiting process to complete
                boolean exitCode = proc.waitFor(100, TimeUnit.MILLISECONDS);
                if (exitCode) {
                    continue;
                } else {
                    // Waiting for more time to complete
                    proc.waitFor(500, TimeUnit.MILLISECONDS);
                }
            }
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    /**
     * Update patch file by adding the patch headers at start of the file
     *
     * @param patchFileName the patch file name
     * @param os the OS name
     * @param patchHeaderContent the patch header content
     * @param patchNo the patch number
     * @return update status
     */
    private static boolean updatePatchFile(File patchFileName, String os, String patchHeaderContent, String patchNo) {
        File patchOldFileName = patchFileName;
        List<String> newLines = new ArrayList<>();

        // Replace patch header by creating new set of lines
        try {
            // Prepend patch to new file lines
            if (os.equalsIgnoreCase(Constants.WINDOWS)) {
                newLines.add(patchHeaderContent.replaceAll(Constants.UNIX_NEW_LINE, Constants.WINDOWS_NEW_LINE));
            } else {
                newLines.add(patchHeaderContent.replaceAll(Constants.WINDOWS_NEW_LINE, Constants.UNIX_NEW_LINE));
            }
            // Get lines without # as starting and add to new file lines
            for (String line : Files.readAllLines(Paths.get(patchOldFileName.getAbsolutePath()))) {
                if (!(line.startsWith("#"))) {
                    newLines.add(line);
                }
            }

            // Rename patch file with patch number
            if (patchOldFileName.getAbsoluteFile().delete()) {
                File patchNoFile = new File(patchOldFileName.getParent(), patchNo.toLowerCase() + Constants.TXT_EXTN);
                Files.write(Paths.get(patchNoFile.getAbsolutePath()), newLines);
            }
        } catch (IOException ex) {
            Logger.getLogger(HomeScreen.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    /**
     * Create new patch file
     *
     * @param patchFolder the patch folder
     * @param os the OS name
     * @param patchHeaderContent the patch header content
     * @param patchNo the patch number
     * @return status of patch file creation
     */
    private static boolean createPatchFile(File patchFolder, String os, String patchHeaderContent, String patchNo) {
        List<String> newLines = new ArrayList<>();
        patchFolder.mkdirs();

        // Get the patch file content
        if (os.equalsIgnoreCase(Constants.WINDOWS)) {
            newLines.add(patchHeaderContent.replaceAll(Constants.UNIX_NEW_LINE, Constants.WINDOWS_NEW_LINE));
        } else {
            newLines.add(patchHeaderContent.replaceAll(Constants.WINDOWS_NEW_LINE, Constants.UNIX_NEW_LINE));
        }
        // Create patch file and add the content
        File patchNoFile = new File(patchFolder, patchNo.toLowerCase() + Constants.TXT_EXTN);
        try {
            Files.write(Paths.get(patchNoFile.getAbsolutePath()), newLines, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            Logger.getLogger(HomeScreen.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    /**
     * Create or update patch file
     *
     * @param patchFolder the patch folder
     * @param os the OS name
     * @param patchHeaderContent the patch header content
     * @param patchNo the patch number
     * @return status of patch file creation or update
     */
    public static boolean createOrUpdatePatch(File patchFolder, String os, String patchHeaderContent, String patchNo) {
        // Check for patch file existance
        if (Files.exists(patchFolder.toPath()) && patchFolder.isDirectory()) {
            // if already exists, replace patch header i.e. till start line character #
            File[] textFileList = FileUtils.getPatchFileNameList(patchFolder, FileUtils.getTextFileNameFilter());
            switch (textFileList.length) {
                case 0:
                    // when no patch text files, create patch file and add the patch header
                    return PatchUtils.createPatchFile(patchFolder, os, patchHeaderContent, patchNo);
                case 1:
                    // when patch text file exists, update with latest patch headers
                    return PatchUtils.updatePatchFile(textFileList[0], os, patchHeaderContent, patchNo);
                default:
                    UiUtils.showErrorMessage(null, Constants.MULTIPLE_PATCH_FILE_ERROR_MSG, Constants.MULTIPLE_PATCH_FILE_ERROR_TITLE);
                    return false;
            }
        } else {
            // else, create patch file and add the patch header
            return PatchUtils.createPatchFile(patchFolder, os, patchHeaderContent, patchNo);
        }
    }
}
