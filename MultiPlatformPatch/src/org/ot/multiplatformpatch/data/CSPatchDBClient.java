/* 
 * Copyright (C) 2017 Suresh Raju Pilli.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package org.ot.multiplatformpatch.data;

/**
 *
 * @author Khasim
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import org.ot.multiplatformpatch.utils.Constants;

/**
 * This class provides methods to retrieve the patch headers and tasks
 * information from CS Patch DB Server
 *
 * @author Suresh Raju Pilli
 */
public class CSPatchDBClient {

    private String baseUrl;
    private String userName;

    /**
     * Constructor with default Base URL and user name
     */
    public CSPatchDBClient() {
        this.baseUrl = Constants.BASE_URL;
        this.userName = Constants.USERNAME;
    }

    /**
     * Constructor with arguments
     *
     * @param baseUrl the Base URL
     * @param userName the Username for authentication
     */
    public CSPatchDBClient(String baseUrl, String userName) {
        this.baseUrl = baseUrl;
        this.userName = userName;
    }

    /**
     * Get user credentials
     *
     * @return the base 64 encoded string of default username
     */
    private String getUserCredentials() {
        return Base64.getEncoder().encodeToString((this.getUserName()).getBytes());
    }

    /**
     * Get User Tasks
     *
     * @param userName the CS DB Username
     * @return ArrayList the list of user tasks
     */
    public ArrayList getUserTasks(String userName) {
        ArrayList tasks = new ArrayList();
        int responseCode;
        String requestUrl;
        HttpURLConnection conn;
        URL url;
        requestUrl = this.getUrl() + "//GetTask?username=" + userName;
        try {
            url = new URL(requestUrl);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Authorization", "Basic " + getUserCredentials());
            responseCode = conn.getResponseCode();
            if (responseCode != HttpURLConnection.HTTP_OK) {
                if (responseCode == HttpURLConnection.HTTP_MOVED_TEMP
                        || responseCode == HttpURLConnection.HTTP_MOVED_PERM) {
                    requestUrl = conn.getHeaderField("Location");
                    // open the new connnection again
                    conn = (HttpURLConnection) new URL(requestUrl).openConnection();
                    conn.setRequestProperty("Authorization", "Basic " + getUserCredentials());
                }
            }
            if (200 == conn.getResponseCode()) {
                //TODO: WRITE THE LOGIC TO BUILD AD RETURN TASKS
            } else {

                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }
            conn.disconnect();

        } catch (IOException | RuntimeException e) {
            System.err.println("Unable to get tasks" + e.getMessage());
            return new ArrayList();
        }
        return tasks;
    }

    /**
     * Get Patch Header with task Id and create xml file in temp directory
     *
     * @param taskid the task ID
     * @return File the created temp file
     */
    public File getPatchHeaders(int taskid) {
        int responseCode;
        String requestUrl;
        HttpURLConnection conn;
        URL url;
        PrintWriter writer;
        requestUrl = this.getUrl() + Constants.PATCHHEADER_URL + taskid;
        File tmpFile = null;

        try {
            url = new URL(requestUrl);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Authorization", "Basic " + getUserCredentials());
            responseCode = conn.getResponseCode();
            if (responseCode != HttpURLConnection.HTTP_OK) {
                if (responseCode == HttpURLConnection.HTTP_MOVED_TEMP
                        || responseCode == HttpURLConnection.HTTP_MOVED_PERM) {
                    requestUrl = conn.getHeaderField("Location");
                    // open the new connnection again
                    conn = (HttpURLConnection) new URL(requestUrl).openConnection();
                    conn.setRequestProperty("Authorization", "Basic " + getUserCredentials());
                }
            }
            if (conn.getResponseCode() == 200) {
                tmpFile = new File(System.getProperty("java.io.tmpdir") + "\\" + Constants.PATCH_HEADER_XML_FILENAME);
                writer = new PrintWriter(tmpFile, Constants.UTF8_CHARSET);

                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));

                String output;

                while ((output = br.readLine()) != null) {
                    writer.println(output);
                }
            } else {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }
            writer.flush();
            writer.close();
            conn.disconnect();
        } catch (IOException | RuntimeException e) {
            System.err.println("Unable to get patch header" + e.getMessage());
        }
        return tmpFile;
    }

    /**
     * @return the baseUrl
     */
    public String getUrl() {
        return baseUrl;
    }

    /**
     * @param baseUrl the baseUrl to set
     */
    public void setUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }
}
