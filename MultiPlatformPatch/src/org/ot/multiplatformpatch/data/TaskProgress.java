/*
 *  Copyright (C) Suresh Raju Pilli from 2017 to present
 * All rights are reserved  * 
 */
package org.ot.multiplatformpatch.data;

/**
 * This enum has task progress information with percentage of task
 *
 * @author Suresh Raju Pilli
 */
public enum TaskProgress {
    CREATE_FOLDERS(10),
    GET_PATCH(10),
    PARSE_PATCH(10),
    UPDATE_WIN_PATCH(10),
    UPDATE_LNX_PATCH(10),
    UPDATE_SOL_PATCH(10),
    CHANGE_LNX_EOL(5),
    CHANGE_SOL_EOL(5),
    CREATE_WIN_ARCHIVE(5),
    CREATE_LNX_ARCHIVE(10),
    CREATE_SOL_ARCHIVE(10),
    DELETE_FOLDERS(5);

    private int progress;

    /**
     * Set the task progress
     *
     * @param progress the progress
     */
    TaskProgress(int progress) {
        this.progress = progress;
    }

    /**
     * Get the progress of the task
     *
     * @return progress the progress
     */
    public int progress() {
        return progress;
    }
}
