/* 
 * Copyright (C) 2017 Suresh Raju Pilli.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */
package org.ot.multiplatformpatch.reader;

import java.io.File;
import java.io.FileFilter;
import java.util.Set;
import org.ot.multiplatformpatch.utils.StringUtils;
import org.ot.multiplatformpatch.utils.UiUtils;

/**
 * This class facilitates users to filter files
 *
 * @author psrdotcom
 */
public class ExcludeFileFilter implements FileFilter {

    private Set<String> extensionsSet;

    /**
     * Constructor with exclude file extensions
     *
     * @param fileName
     */
    public ExcludeFileFilter(File fileName) {
        extensionsSet = UiUtils.filesToExclude(fileName);
    }

    @Override
    public boolean accept(File file) {
        if (file == null) {
            return false;
        } else {
            // Check for system files
            if (file.getName().startsWith(".") && file.isHidden()) {
                return false;
            }

            // Exclude file extension list
            String fileExtn = StringUtils.getFileExtension(file);
            if (fileExtn != null && extensionsSet.contains(fileExtn)) {
                return false;
            }

            // Otherwise, return true
            return true;
        }
    }

}
